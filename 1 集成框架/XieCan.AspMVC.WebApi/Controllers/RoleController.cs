﻿using System.Collections.Generic;
using XieCan.AspMVC.ViewModels;
using XieCan.AspMVC.Models;
using System;

namespace XieCan.AspMVC.WebApi.Controllers
{
    /// <summary>
    /// 角色管理
    /// </summary>
    [ApiAuthorize]
    public class RoleController : BaseController
    {
        /// <summary>
        /// 获取所有角色信息
        /// </summary>
        /// <returns></returns>
        public OperateResult<IEnumerable<RoleViewModel>> GetList()
        {
            var result = RoleService.Select();
            return MapResult<RoleModel, RoleViewModel>(result);
        }

        /// <summary>
        /// 获取单个角色信息
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public OperateResult<RoleViewModel> GetFirst(string guid = "24222940-7C21-43AE-AF7D-CFF3F730B7AF")
        {
            var result = RoleService.Find(Guid.Parse(guid));
            return MapResult<RoleModel, RoleViewModel>(result);
        }
    }
}
