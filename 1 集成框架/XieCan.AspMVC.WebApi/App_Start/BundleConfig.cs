﻿using System.Web;
using System.Web.Optimization;

namespace XieCan.AspMVC.WebApi
{
    public class BundleConfig
    {
        // 有关捆绑的详细信息，请访问 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                      "~/Content/bootstrap/bootstrap-{version}.css"));

            bundles.Add(new StyleBundle("~/Content/prettify").Include(
                      "~/Content/prettify/prettify.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Content/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/dbshare").Include(
                      "~/Content/extensions/bdshare.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                      "~/Content/modernizr/modernizr-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap/bootstrap-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/prettify").Include(
                      "~/Content/prettify/prettify.js"));
        }
    }
}
