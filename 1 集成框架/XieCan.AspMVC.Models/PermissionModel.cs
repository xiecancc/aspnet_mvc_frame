﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 权限信息类
    /// </summary>
    [Table("Permissions")]
    [XieCanT6Type(Summary = "权限信息", Name = "Permission")]
    public class PermissionModel : BaseModel
    {
        /// <summary>
        /// 所属区域
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Area { get; set; }

        /// <summary>
        /// 所属控制器
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Controller { get; set; }

        /// <summary>
        /// 所属动作
        /// </summary>
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Action { get; set; }

        /// <summary>
        /// 所属方式
        /// </summary>
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Method { get; set; }

        /// <summary>
        /// 角色信息
        /// </summary>
        public virtual ICollection<RoleModel> Roles { get; set; } = new HashSet<RoleModel>();
    }
}
