﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 角色信息类
    /// </summary>
    [Table("Roles")]
    [XieCanT6Type(Summary = "角色信息", Name = "Role")]
    public class RoleModel : BaseModel
    {
        /// <summary>
        /// 角色名
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        /// <summary>
        /// 角色说明
        /// </summary>
        [MaxLength(50)]
        public string Comment { get; set; }

        /// <summary>
        /// 账号信息
        /// </summary>
        public virtual ICollection<AccountModel> Accounts { get; set; } = new HashSet<AccountModel>();

        /// <summary>
        /// 权限信息
        /// </summary>
        public virtual ICollection<PermissionModel> Permissions { get; set; } = new HashSet<PermissionModel>();
    }
}
