﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 文章分类类
    /// </summary>
    [Table("ArticleType")]
    [XieCanT6Type(Summary = "文章分类", Name = "ArticleType")]
    public class ArticleTypeModel : BaseModel
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Index("UIX_Name", IsUnique = true)]
        public string Name { get; set; }

        /// <summary>
        /// 文章数量
        /// </summary>
        [NotMapped]
        public long Count
        {
            get
            {
                return Articles.LongCount();
            }
        }

        /// <summary>
        /// 文章列表
        /// </summary>
        public virtual ICollection<ArticleModel> Articles { get; set; } = new HashSet<ArticleModel>();
    }
}
