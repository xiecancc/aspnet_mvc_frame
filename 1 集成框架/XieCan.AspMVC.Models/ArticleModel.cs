﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 文章信息类
    /// </summary>
    [Table("Article")]
    [XieCanT6Type(Summary = "文章信息", Name = "Article")]
    public class ArticleModel : BaseModel
    {
        /// <summary>
        /// 文章标题
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// 文章作者
        /// </summary>
        [Required]
        [MaxLength(20)]
        public string Author { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Required]
        [Column(TypeName = "ntext")]
        public string Context { get; set; }

        /// <summary>
        /// 所属类别
        /// </summary>
        public virtual ICollection<ArticleTypeModel> ArticleTypes { get; set; } = new HashSet<ArticleTypeModel>();
    }
}
