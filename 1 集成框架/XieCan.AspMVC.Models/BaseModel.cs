﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 实体类的基类，提供共有属性
    /// </summary>
    public abstract class BaseModel : XieCanBase
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; } = new Guid();

        /// <summary>
        /// 添加时间
        /// </summary>
        public virtual DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 是否删除
        /// </summary>
        public virtual bool IsDeleted { get; set; } = false;

        /// <summary>
        /// 版本号
        /// </summary>
        [Timestamp]
        public virtual byte[] Version { get; set; }

        //[NotMapped]
        //public string OrderId => Id.ToString();
    }
}
