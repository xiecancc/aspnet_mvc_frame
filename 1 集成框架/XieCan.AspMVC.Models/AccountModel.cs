﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 账号信息类
    /// </summary>
    [Table("AccountInfo")]
    [XieCanT6Type(Summary = "账号信息", Name = "Account")]
    public class AccountModel : BaseModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Required]
        [MaxLength(20)]
        [Column(TypeName = "varchar")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        [MaxLength(32)]
        [Column(TypeName = "varchar")]
        public string Password { get; set; }

        /// <summary>
        /// 加盐加密
        /// </summary>
        [Required]
        [MaxLength(6)]
        [Column(TypeName = "varchar")]
        public string Salt { get; set; }

        /// <summary>
        /// 最近一次登录时间
        /// </summary>
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// 登录错误次数
        /// </summary>
        public int ErrorTimes { get; set; }

        /// <summary>
        /// 基本信息
        /// </summary>
        [ForeignKey("Id")]
        public virtual UserInfoModel UserInfo { get; set; }

        /// <summary>
        /// 所属角色
        /// </summary>
        public virtual RoleModel Role { get; set; }
    }
}
