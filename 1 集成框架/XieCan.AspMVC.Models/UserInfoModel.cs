﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Models
{
    /// <summary>
    /// 基本信息类
    /// </summary>
    [Table("UserInfo")]
    [XieCanT6Type(Summary = "基本信息", Name = "UserInfo")]
    public class UserInfoModel : BaseModel
    {
        /// <summary>
        /// 编号
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override Guid Id { get => base.Id; set => base.Id = value; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public bool? Sex { get; set; } = null;

        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 账号信息
        /// </summary>
        [ForeignKey("Id")]
        [Required]
        public virtual AccountModel Account { get; set; }
    }
}
