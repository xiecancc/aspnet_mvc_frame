﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.ViewModels
{
    [XieCanT6Mapper(typeof(AccountModel))]
    public class SimpleLoginViewModel : BaseViewModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName("密码")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    [XieCanT6Mapper(typeof(AccountModel))]
    public class LoginViewModel : SimpleLoginViewModel
    {
        /// <summary>
        /// 验证码
        /// </summary>
        [DisplayName("验证码")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Code { get; set; }

        /// <summary>
        /// 记住我
        /// </summary>
        [DisplayName("记住我？")]
        public bool RemberMe { get; set; }
    }

    [XieCanT6Mapper(typeof(AccountModel))]
    public class LoginResultViewModel : BaseViewModel
    {
        /// <summary>
        /// 账号
        /// </summary>
        [DisplayName("账号")]
        public string Account { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        [DisplayName("注册时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 最近一次登录时间
        /// </summary>
        [DisplayName("最近一次登录时间")]
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// 所属角色
        /// </summary>
        public RoleViewModel Role { get; set; }
    }
}
