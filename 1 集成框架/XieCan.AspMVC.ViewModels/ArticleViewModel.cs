﻿using System.ComponentModel.DataAnnotations;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.ViewModels
{
    [XieCanT6Mapper(typeof(ArticleModel))]
    public class ArticleSelectViewModel : BaseViewModel
    {
        /// <summary>
        /// 文章标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 文章作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public string CreateDate { get; set; }
    }

    [XieCanT6Mapper(typeof(ArticleModel))]
    public class ArticleInsertViewModel : BaseViewModel
    {
        /// <summary>
        /// 文章标题
        /// </summary>
        [Display(Name = "文章标题")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [MaxLength(50, ErrorMessage = "{0} 不能超过{1}个字符")]
        public string Title { get; set; }

        /// <summary>
        /// 文章作者
        /// </summary>
        [Display(Name = "文章作者")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [MaxLength(20, ErrorMessage = "{0} 不能超过{1}个字符")]
        public string Author { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Display(Name = "文章内容")]
        [Required(ErrorMessage = "{0} 不能为空")]
        public string Context { get; set; }
    }
}
