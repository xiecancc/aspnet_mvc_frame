﻿using System;
using System.ComponentModel.DataAnnotations;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.ViewModels
{
    [XieCanT6Mapper(typeof(ArticleTypeModel))]
    public class ArticleTypeSelectViewModel : BaseViewModel
    {
        [Display(Name = "编号")]
        public Guid Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [Display(Name = "分类名称")]
        public string Name { get; set; }

        /// <summary>
        /// 文章数量
        /// </summary>
        [Display(Name = "文章数量")]
        public long Count { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [Display(Name = "是否删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        [Display(Name = "添加时间")]
        public DateTime CreateTime { get; set; }
    }

    [XieCanT6Mapper(typeof(ArticleTypeModel))]
    public class ArticleTypeUpdateViewModel : BaseViewModel
    {
        public Guid Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [Display(Name = "分类名称")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [MaxLength(20, ErrorMessage = "{0} 不能超过{1}个字符")]
        public string Name { get; set; }
    }

    [XieCanT6Mapper(typeof(ArticleTypeModel))]
    public class ArticleTypeInsertViewModel : BaseViewModel
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        [Display(Name = "分类名称")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [MaxLength(20, ErrorMessage = "{0} 不能超过{1}个字符")]
        public string Name { get; set; }
    }
}
