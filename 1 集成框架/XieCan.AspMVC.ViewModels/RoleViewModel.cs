﻿using System;
using System.ComponentModel.DataAnnotations;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.ViewModels
{
    [XieCanT6Mapper(typeof(RoleModel))]
    public class RoleViewModel : BaseViewModel
    {
        /// <summary>
        /// 角色编号
        /// </summary>
        [Display(Name = "角色编号")]
        public Guid ID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [Display(Name = "角色名称")]
        [Required(ErrorMessage = "{0} 不能为空")]
        [StringLength(20, ErrorMessage = "{0} 应该介于{2}-{1}之间")]
        public string Name { get; set; }
    }
}
