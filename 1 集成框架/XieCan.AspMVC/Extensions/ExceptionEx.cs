﻿using System;

namespace XieCan.AspMVC.Extensions
{
    /// <summary>
    /// Exception系统异常扩展类
    /// </summary>
    public static class ExceptionEx
    {
        /// <summary>
        /// 获取最内部的异常信息
        /// </summary>
        /// <param name="exception">当前错误</param>
        /// <returns></returns>
        public static Exception DepthException(this Exception exception)
        {
            if (exception == null)
            {
                return null;
            }

            if (exception.InnerException == null)
            {
                return exception;
            }

            do
            {
                exception = exception.InnerException;
            } while (exception.InnerException != null);

            return exception;
        }
    }
}
