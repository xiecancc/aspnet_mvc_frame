﻿using System;

namespace XieCan.AspMVC.Reflections
{
    /// <summary>
    /// T6自动生成代码的方法约束
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class XieCanT6Method : XieCanT6Base
    {
        /// <summary>
        /// 方法名
        /// </summary>
        public override string Name { get => base.Name; set => base.Name = value; }

        /// <summary>
        /// 方法注释
        /// </summary>
        public override string Summary { get => base.Summary; set => base.Summary = value; }

        /// <summary>
        /// 泛型方法的实体类型
        /// </summary>
        public override Type Model { get => base.Model; set => base.Model = value; }

        /// <summary>
        /// 方法的参数集合
        /// </summary>
        public string Paramters { get; set; }

        /// <summary>
        /// 方法的返回值类型
        /// </summary>
        public string ReturnType { get; set; }
    }
}
