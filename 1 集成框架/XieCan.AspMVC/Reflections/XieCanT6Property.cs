﻿using System;

namespace XieCan.AspMVC.Reflections
{
    /// <summary>
    /// T6自动生成代码的属性约束
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class XieCanT6Property : XieCanT6Base
    {
        /// <summary>
        /// 属性类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 属性名
        /// </summary>
        public override string Name { get => base.Name; set => base.Name = value; }

        /// <summary>
        /// 属性注释
        /// </summary>
        public override string Summary { get => base.Summary; set => base.Summary = value; }

        /// <summary>
        /// 泛型属性的实体类型
        /// </summary>
        public override Type Model { get => base.Model; set => base.Model = value; }
    }
}
