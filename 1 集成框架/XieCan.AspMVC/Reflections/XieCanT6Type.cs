﻿using System;

namespace XieCan.AspMVC.Reflections
{
    /// <summary>
    /// T6自动生成代码的类或接口约束
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = true)]
    public class XieCanT6Type : XieCanT6Base
    {
        /// <summary>
        /// 类型说明
        /// </summary>
        public override string Summary { get => base.Summary; set => base.Summary = value; }

        /// <summary>
        /// 类型名称
        /// </summary>
        public override string Name { get => base.Name; set => base.Name = value; }

        /// <summary>
        /// 基类名称
        /// </summary>
        public string Parent { get; set; }

        /// <summary>
        /// 泛型类的实体类型
        /// </summary>
        public override Type Model { get => base.Model; set => base.Model = value; }
    }
}
