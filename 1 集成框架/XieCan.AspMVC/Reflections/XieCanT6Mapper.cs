﻿using System;

namespace XieCan.AspMVC.Reflections
{
    /// <summary>
    /// T6自动生成AutoMapper映射配置的类或接口约束
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class XieCanT6Mapper : XieCanT6Base
    {
        public XieCanT6Mapper(params Type[] type)
        {
            Model = type;
        }

        /// <summary>
        /// 转换类型
        /// </summary>
        public new Type[] Model { get; set; }
    }
}
