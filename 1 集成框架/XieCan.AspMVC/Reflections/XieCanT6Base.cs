﻿using System;

namespace XieCan.AspMVC.Reflections
{
    public abstract class XieCanT6Base : Attribute
    {
        public virtual string Summary { get; set; }

        public virtual string Name { get; set; }

        public virtual Type Model { get; set; }
    }
}
