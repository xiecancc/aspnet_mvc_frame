/*
    作者：谢灿软件
    企鹅：492384481
    时间：2020-11-25 13:52:00
    说明：
        1.此文件根据XieCan.AspMVC.IService项目中所有XieCanT6Type约束的接口自动生成
        2.需要提前编译XieCan.AspMVC.IService项目，否则将生成失败或生成的代码不正确
        3.生成代码为partial类，支持开发人员创建同名partial类进行代码合并
*/
using XieCan.AspMVC.Log4net;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.IService;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Service
{
    /// <summary>
    /// 日志信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "日志信息", Model = typeof(Log4Model))]
    public partial class Log4Service : BaseService<Log4Model>, ILog4Service
    {
        private ILog4Service Service => this;
    }
     /// <summary>
    /// 账号信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "账号信息", Model = typeof(AccountModel))]
    public partial class AccountService : BaseService<AccountModel>, IAccountService
    {
        private IAccountService Service => this;
    }
     /// <summary>
    /// 文章信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "文章信息", Model = typeof(ArticleModel))]
    public partial class ArticleService : BaseService<ArticleModel>, IArticleService
    {
        private IArticleService Service => this;
    }
     /// <summary>
    /// 文章分类操作类
    /// </summary>
    [XieCanT6Type(Summary = "文章分类", Model = typeof(ArticleTypeModel))]
    public partial class ArticleTypeService : BaseService<ArticleTypeModel>, IArticleTypeService
    {
        private IArticleTypeService Service => this;
    }
     /// <summary>
    /// 权限信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "权限信息", Model = typeof(PermissionModel))]
    public partial class PermissionService : BaseService<PermissionModel>, IPermissionService
    {
        private IPermissionService Service => this;
    }
     /// <summary>
    /// 角色信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "角色信息", Model = typeof(RoleModel))]
    public partial class RoleService : BaseService<RoleModel>, IRoleService
    {
        private IRoleService Service => this;
    }
     /// <summary>
    /// 基本信息操作类
    /// </summary>
    [XieCanT6Type(Summary = "基本信息", Model = typeof(UserInfoModel))]
    public partial class UserInfoService : BaseService<UserInfoModel>, IUserInfoService
    {
        private IUserInfoService Service => this;
    }
    
}
