﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using XieCan.AspMVC.Extensions;
using XieCan.AspMVC.Models;

namespace XieCan.AspMVC.Log4net
{
    /// <summary>
    /// Log4net日志操作类
    /// </summary>
    [Table("Log4")]
    public class Log4Model : BaseModel
    {
        public Log4Model()
        {
            var Request = HttpContext.Current.Request;
            //服务端获取IP地址
            string userIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrWhiteSpace(userIP))
            {
                userIP = Request.ServerVariables["REMOTE_ADDR"];
            }
            if (string.IsNullOrWhiteSpace(userIP))
            {
                userIP = Request.UserHostAddress;
            }
            RequestIP = userIP;
            RequestUrl = Request.RawUrl;
            RequestMethod = Request.RequestType;
            RequestData = Request.Form.ToJson().Data;
            RequestAgent = Request.UserAgent;
        }

        /// <summary>
        /// 所属进程
        /// </summary>
        [Display(Name = "所属进程")]
        [Column(TypeName = "varchar"), StringLength(255)]
        public virtual string Thread { get; set; }

        /// <summary>
        /// 日志级别
        /// </summary>
        [Display(Name = "日志级别")]
        [Column(TypeName = "varchar"), StringLength(50)]
        public virtual string Level { get; private set; } = Log4Type.Info.ToString();

        /// <summary>
        /// 日志级别
        /// </summary>
        [NotMapped]
        public virtual Log4Type LogType
        {
            get
            {
                return (Log4Type)Enum.Parse(typeof(Log4Type), Level, true);
            }
            set
            {
                Level = value.ToString();
            }
        }

        /// <summary>
        /// 日志内容
        /// </summary>
        [Display(Name = "日志内容")]
        [Column(TypeName = "varchar"), StringLength(4000)]
        public virtual string Message { get; set; }

        /// <summary>
        /// 异常详情
        /// </summary>
        [Display(Name = "异常详情")]
        [Column(TypeName = "varchar"), StringLength(2000)]
        public string Exception { get; set; }

        /// <summary>
        /// 请求时间
        /// </summary>
        [Display(Name = "请求时间")]
        [Column("Date")]
        public override DateTime CreateTime { get => base.CreateTime; set => base.CreateTime = value; }

        /// <summary>
        /// 用户标识
        /// </summary>
        [Display(Name = "用户标识")]
        [Column(TypeName = "char"), StringLength(36)]
        public string UserId { get; set; }

        /// <summary>
        /// 用户IP
        /// </summary>
        [Display(Name = "用户IP")]
        [Column(TypeName = "varchar"), StringLength(15)]
        public string RequestIP { get; private set; }

        /// <summary>
        /// 请求地址
        /// </summary>
        [Display(Name = "请求地址")]
        [Column(TypeName = "varchar")]
        public string RequestUrl { get; private set; }

        /// <summary>
        /// 请求参数(POST)
        /// </summary>
        [Display(Name = "POST参数")]
        [Column(TypeName = "varchar")]
        public string RequestData { get; private set; }

        /// <summary>
        /// 请求方式
        /// </summary>
        [Display(Name = "请求方式")]
        [Column(TypeName = "varchar")]
        public string RequestMethod { get; private set; }

        /// <summary>
        /// 用户代理
        /// </summary>
        [Display(Name = "用户代理")]
        [Column(TypeName = "varchar")]
        public string RequestAgent { get; private set; }
    }
}
