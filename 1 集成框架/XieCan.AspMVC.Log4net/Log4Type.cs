﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XieCan.AspMVC.Log4net
{
    /// <summary>
    /// Log4日志类型
    /// </summary>
    public enum Log4Type
    {
        Debug = 0,
        Info = 1,
        Warn = 2,
        Error = 4,
        Fatal = 8
    }
}
