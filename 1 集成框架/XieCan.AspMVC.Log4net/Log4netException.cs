﻿using log4net;
using System.Web.Mvc;

namespace XieCan.AspMVC.Log4net
{
    public class Log4netException : IExceptionFilter
    {
        protected static ILog Log => LogManager.GetLogger(typeof(Log4netException));

        public void OnException(ExceptionContext filterContext)
        {
            Log.Log4("未处理异常", type: Log4Type.Fatal, exception: filterContext.Exception);
        }
    }
}
