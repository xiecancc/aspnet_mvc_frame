﻿using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;
using Autofac.Integration.Mvc;
using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using XieCan.AspMVC.Helpers;

namespace XieCan.AspMVC.Autofac
{
    public class AutofacHelper
    {
        /// <summary>
        /// 获取线程内唯一ContainerBuilder对象
        /// </summary>
        private static ContainerBuilder Container => CallContextHelper.Single<ContainerBuilder>();

        /// <summary>
        /// 执行注入
        /// </summary>
        public static void RegisterAllTypes()
        {
            var container = Container.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        /// <summary>
        /// 追加程序集
        /// </summary>
        /// <param name="assemblies">程序集</param>
        /// <param name="filter">注入类型过滤器</param>
        /// <param name="interfaces">是否注入为接口</param>
        /// <param name="roperties">是否为属性注入</param>
        /// <returns></returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle> AppendAssembly(Func<Type, bool> filter = null, bool interfaces = false, bool roperties = true, params Assembly[] assemblies)
        {
            var result = Container.RegisterAssemblyTypes(assemblies);

            if (filter != null)
                result.Where(filter);

            if (interfaces)
                result.AsImplementedInterfaces();

            if (roperties)
                result.PropertiesAutowired();

            return result;
        }
    }
}
