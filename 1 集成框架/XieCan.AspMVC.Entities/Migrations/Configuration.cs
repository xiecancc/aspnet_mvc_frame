﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using XieCan.AspMVC.Helpers;
using XieCan.AspMVC.Models;

namespace XieCan.AspMVC.Entities.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<EntitiesContext>
    {
        public Configuration()
        {
            // 启用自动迁移
            AutomaticMigrationsEnabled = true;
            // 显式开启允许修改表结构
            // 更改数据库中结构(增加、删除列、修改列、改变列的属性、增加、删除、修改表),需要显示开启。
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(EntitiesContext context)
        {
            var role = context.Roles.FirstOrDefault(p => p.Name == "Admin");
            if (role == null)
            {
                role = new RoleModel()
                {
                    Name = "Admin",
                    Comment = "超级管理员"
                };
                context.Roles.Add(role);
            }
            role.Accounts.Add(new AccountModel()
            {
                Account = "492384481@qq.com",
                Salt = "XieCan",
                Password = MD5Helper.MD5Encoding("XieCan.cc", "XieCan")
            });
            role = context.Roles.FirstOrDefault(p => p.Name == "Manager");
            if (role == null)
            {
                role = new RoleModel()
                {
                    Name = "Manager",
                    Comment = "普通管理员"
                };
                context.Roles.Add(role);
            }
            role = context.Roles.FirstOrDefault(p => p.Name == "VIP");
            if (role == null)
            {
                role = new RoleModel()
                {
                    Name = "VIP",
                    Comment = "普通会员"
                };
                context.Roles.Add(role);
            }
            context.SaveChanges();
        }
    }
}
