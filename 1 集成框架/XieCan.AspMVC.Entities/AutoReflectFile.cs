/*
    作者：谢灿软件
    企鹅：492384481
    时间：2020-12-03 19:38:00
    说明：
        1.此文件根据XieCan.AspMVC.Models项目中所有XieCanT6Type约束的实体类自动生成
        2.需要提前编译XieCan.AspMVC.Models项目，否则将生成失败或生成的代码不正确
        2.生成代码为partial EntitiesContext类，支持开发人员创建同名partial EntitiesContext进行代码合并
*/
using System.Data.Entity;
using XieCan.AspMVC.Log4net;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.Entities
{
    public partial class EntitiesContext
    {
        /// <summary>
        /// 日志信息
        /// </summary>
        [XieCanT6Property(Summary = "日志信息", Name = "Log4", Model = typeof(Log4Model))]
        public virtual DbSet<Log4Model> Logs { get; set; }
    
        /// <summary>
        /// 账号信息
        /// </summary>
        [XieCanT6Property(Summary = "账号信息", Name = "Account", Model = typeof(AccountModel))]
        public virtual DbSet<AccountModel> Accounts { get; set; }
    
        /// <summary>
        /// 文章信息
        /// </summary>
        [XieCanT6Property(Summary = "文章信息", Name = "Article", Model = typeof(ArticleModel))]
        public virtual DbSet<ArticleModel> Articles { get; set; }
    
        /// <summary>
        /// 文章分类
        /// </summary>
        [XieCanT6Property(Summary = "文章分类", Name = "ArticleType", Model = typeof(ArticleTypeModel))]
        public virtual DbSet<ArticleTypeModel> ArticleTypes { get; set; }
    
        /// <summary>
        /// 权限信息
        /// </summary>
        [XieCanT6Property(Summary = "权限信息", Name = "Permission", Model = typeof(PermissionModel))]
        public virtual DbSet<PermissionModel> Permissions { get; set; }
    
        /// <summary>
        /// 角色信息
        /// </summary>
        [XieCanT6Property(Summary = "角色信息", Name = "Role", Model = typeof(RoleModel))]
        public virtual DbSet<RoleModel> Roles { get; set; }
    
        /// <summary>
        /// 基本信息
        /// </summary>
        [XieCanT6Property(Summary = "基本信息", Name = "UserInfo", Model = typeof(UserInfoModel))]
        public virtual DbSet<UserInfoModel> UserInfos { get; set; }
    }
}
