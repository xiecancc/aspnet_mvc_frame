﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using XieCan.AspMVC.Entities.Migrations;
using XieCan.AspMVC.Log4net;

namespace XieCan.AspMVC.Entities
{
    /// <summary>
    /// EF数据集
    /// </summary>
    public partial class EntitiesContext : DbContext
    {
        /// <summary>
        /// 静态构造函数，启用EF的自动迁移
        /// </summary>
        static EntitiesContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EntitiesContext, Configuration>("Entities"));
        }

        public EntitiesContext() : base("name=Entities")
        {
            Database.Log = (sql) =>
            {
                //this.Log4("EF开始执行sql语句：" + sql);
            };
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 移除生成数据表的复数形式
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            // 移除一对多的级联删除
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            // 移除多对多的级联删除
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}
