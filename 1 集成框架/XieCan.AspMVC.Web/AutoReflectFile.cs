/*
    作者：谢灿软件
    企鹅：492384481
    时间：2020-12-05 12:44:48
    说明：
        1.此文件根据XieCan.AspMVC.IService项目中所有XieCanT6Type约束的接口自动生成
        2.需要提前编译XieCan.AspMVC.IService项目，否则将生成失败或生成的代码不正确
        3.生成代码为partial类，支持开发人员创建同名partial类进行代码合并
*/
using System.Web.Mvc;
using XieCan.AspMVC.IService;

namespace XieCan.AspMVC.Web
{
    public partial class BaseController : Controller
    {
        
        /// <summary>
        /// 日志信息操作接口
        /// </summary>
        public ILog4Service Log4Service { get; set; }
         
        /// <summary>
        /// 账号信息操作接口
        /// </summary>
        public IAccountService AccountService { get; set; }
         
        /// <summary>
        /// 文章信息操作接口
        /// </summary>
        public IArticleService ArticleService { get; set; }
         
        /// <summary>
        /// 文章分类操作接口
        /// </summary>
        public IArticleTypeService ArticleTypeService { get; set; }
         
        /// <summary>
        /// 权限信息操作接口
        /// </summary>
        public IPermissionService PermissionService { get; set; }
         
        /// <summary>
        /// 角色信息操作接口
        /// </summary>
        public IRoleService RoleService { get; set; }
         
        /// <summary>
        /// 基本信息操作接口
        /// </summary>
        public IUserInfoService UserInfoService { get; set; }
     }
}
