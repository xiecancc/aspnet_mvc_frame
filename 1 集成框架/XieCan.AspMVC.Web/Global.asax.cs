using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using XieCan.AspMVC.Autofac;
using XieCan.AspMVC.Log4net;

namespace XieCan.AspMVC.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //初始化log4net
            log4net.Config.XmlConfigurator.Configure();
            //增加自定义异常处理器
            GlobalFilters.Filters.Add(new Log4netException());

            //为所有Controller注入Service
            AutofacHelper.AppendAssembly(assemblies: typeof(MvcApplication).Assembly, filter: type => type.BaseType == typeof(BaseController));
            AutofacHelper.AppendAssembly(assemblies: Assembly.Load("XieCan.AspMVC.Service"), interfaces: true);
            AutofacHelper.RegisterAllTypes();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
