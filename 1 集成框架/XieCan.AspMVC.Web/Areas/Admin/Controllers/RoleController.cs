﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XieCan.AspMVC.IService;
using XieCan.AspMVC.Mapper;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.ViewModels;

namespace XieCan.AspMVC.Web.Areas.Admin.Controllers
{
    public class RoleController : BaseController
    {
        // GET: Admin/Role
        public ActionResult Index()
        {
            var result = RoleService.Select();
            if (result.Success)
            {
                var data = result.Data.MapTo<RoleViewModel>();
                return View(data);
            }
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name")] RoleViewModel role)
        {
            var result = RoleService.Insert(role.MapTo<RoleModel>());
            if (result.Success)
            {
                return Json(true);
            }
            return Json(false);
        }
    }
}