﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using XieCan.AspMVC.Mapper;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.ViewModels;

namespace XieCan.AspMVC.Web.Areas.Admin.Controllers
{
    public class ArticleTypeController : BaseController
    {
        // GET: Admin/ArticleType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Insert()
        {
            return PartialView();
        }

        public ActionResult Update(string id)
        {
            var result = ArticleTypeService.Find(new System.Guid(id));
            return PartialView(result.Data.MapTo<ArticleTypeUpdateViewModel>());
        }

        public ActionResult Detail(string id)
        {
            var result = ArticleTypeService.Find(new System.Guid(id));
            return PartialView(result.Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ArticleTypeUpdateViewModel model)
        {
            ArticleTypeService.Update("Name", model.MapTo<ArticleTypeModel>());
            var result = ArticleService.SaveChanges();
            return Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insert(ArticleTypeInsertViewModel model)
        {
            ArticleTypeService.Insert(model.MapTo<ArticleTypeModel>());
            var result = ArticleService.SaveChanges();
            return Json(result);
        }

        [HttpPost]
        public ActionResult Delete(params string[] ids)
        {
            ArticleTypeService.Delete(ids);
            var result = ArticleService.SaveChanges();
            return Json(result);
        }

        public ActionResult Select(long pageIndex = 1, int pageSize = 10)
        {
            var result = ArticleTypeService.Select(p => true, p => p.CreateTime, out long rows, out long pages, isAsc: false, pageIndex: pageIndex, pageSize: pageSize);
            if (result.Success)
            {
                var list = result.Data.ToList().MapTo<ArticleTypeSelectViewModel>();
                return Json(new { rows = list, total = rows }, JsonRequestBehavior.AllowGet);
            }
            return Json(result.Message, JsonRequestBehavior.AllowGet);
        }
    }
}