﻿using System.Web.Mvc;

namespace XieCan.AspMVC.Web.Areas.Admin.Controllers
{
    public class VoiceController : BaseController
    {
        // GET: Admin/Voice
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(string name, string content)
        {
            return View();
        }
    }
}