﻿(function ($) {
    $.extend({
        bootstrap: {
            modal: function (modal) {
                var modals = {
                    title: "",
                    content: "",
                    footer: "",
                    page: { type: "get", url: "" },
                    style: { top: "30%", left: "0" },
                    events: {
                        show: function () { },
                        shown: function () { },
                        closed: function () { },
                        closing: function () { }
                    }
                }
                $.extend(true, modals, modal);
                var $modal = $('<div class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true"></div>').on("hide.bs.modal", function () { if (typeof modals.events.closing === "function") { return modals.events.closing(); } }).on("hidden.bs.modal", function () { if (typeof modals.events.closed === "function") { modals.events.closed(); } $modal.remove(); }).on("show.bs.modal", function () { if (typeof modals.events.show === "function") { modals.events.show(); } }).on("shown.bs.modal", function () {
                    if (typeof modals.events.shown === "function") {
                        $modal.find("form").submit(function () {
                            var $this = $(this);
                            if ($.isEmptyObject($this.validate().invalid)) {
                                $.post($this.attr("action"), $this.serializeArray(), function (res) {
                                    $modal.modal("hide");
                                    if (res.state === true) {
                                        $.bootstrap.alert({ type: "success", title: "操作成功" })
                                    } else {
                                        $.bootstrap.alert({ type: "error", title: "操作失败", content: res.message })
                                    }
                                })
                                return false;
                            }
                        })
                        modals.events.shown();
                    }
                    return $modal;
                });
                var $content = $('<div class="modal-content" style="border-radius: 3px;"></div>').append($('<div class="modal-header" style="border-bottom:none"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><div class="text-center" style="margin:0px">' + modals.title + '</div></div>'))
                var page = modals.page;
                if (page.url !== "") {
                    $.ajax({
                        type: page.type,
                        url: page.url,
                        async: false,
                        success: function (result) {
                            modals.content = result;
                        },
                        error: function (error) {
                            modals.content = error;
                        }
                    })
                }
                if (modals.content !== "") {
                    $content.append('<div class="modal-body">' + modals.content + '</div>').find(".modal-header").css("border-bottom", "1px solid #e5e5e5");
                }
                if (modals.footer !== "")
                    $content.append('<div class="modal-footer small text-muted" style="text-align:left;">' + modals.footer + '</div>')
                return $modal.append($('<div class="modal-dialog"></div>').css(modals.style).append($content)).appendTo($("body").css("padding-right", "0")).modal("show");
            },
            alert: function (alert) {
                var alerts = {
                    type: "default",
                    title: "",
                    content: "",
                    style: { top: "30%", left: "0" },
                    events: {
                        show: function () { },
                        shown: function () { },
                        closed: function () { },
                        closing: function () { }
                    }
                }
                $.extend(true, alerts, alert);
                this.modal({ title: "", content: "", footer: "", style: alerts.style, events: alerts.events }).find(".modal-content").css('border-radius', '6px').html('<div class="alert alert-' + alerts.type + '" style="border-radius: 6px;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="alert-heading text-center" style="margin-top: 10px;padding-bottom:10px;">' + alerts.title + '</h4><div>' + alerts.content + '</div></div >')
            },
            confirm: function (confirm) {
                var confirms = {
                    title: "",
                    content: "",
                    style: { top: "30%", left: "0" },
                    events: {
                        show: function () { },
                        shown: function () { },
                        closed: function (result) { }
                    }
                }
                $.extend(true, confirms, confirm);
                this.modal({ title: confirms.title, content: confirms.content, footer: '<div class="pull-right"><button type="button" class="btn btn-default" data-dismiss="modal">取消</button><button type="button" class="btn btn-primary btn-ok" data-dismiss="modal">确定</button></div>', style: confirms.style }).find(".btn").click(function () { confirms.events.closed($(this).hasClass("btn-ok")); });
            }
        }
    })
})(jQuery)