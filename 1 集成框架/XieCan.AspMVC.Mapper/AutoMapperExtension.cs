﻿using System.Collections.Generic;

namespace XieCan.AspMVC.Mapper
{
    public static partial class AutoMapperExtension
    {
        /// <summary>
        ///  类型映射,两个类型的字段名字一一对应
        /// </summary>
        /// <typeparam name="Model">转化之后的类型</typeparam>
        /// <param name="source">可以使用这个扩展方法的类型，任何引用类型</param>
        /// <returns>转化之后的实体</returns>
        public static Model MapTo<Model>(this XieCanBase source)
            where Model : XieCanBase
        {
            return Maps.Map<Model>(source);
        }

        /// <summary>
        /// 集合列表类型映射,两个类型的字段名字一一对应
        /// </summary>
        /// <typeparam name="Model">转化之后的类型</typeparam>
        /// <param name="source">可以使用这个扩展方法的类型，任何引用类型</param>
        /// <returns>转化之后的实体列表</returns>
        public static IEnumerable<Model> MapTo<Model>(this IEnumerable<XieCanBase> source)
            where Model : XieCanBase
        {
            return Maps.Map<IEnumerable<XieCanBase>, IList<Model>>(source);
        }
    }
}
