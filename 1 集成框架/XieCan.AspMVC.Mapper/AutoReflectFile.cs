/*
    作者：谢灿软件
    企鹅：492384481
    时间：2020-12-09 13:10:12
    说明：
        1.此文件根据XieCan.AspMVC.ViewModels项目中所有XieCanT6Mapper约束的类自动生成
        2.需要提前编译XieCan.AspMVC.ViewModels项目，否则将生成失败或生成的代码不正确
        3.生成代码为partial类，支持开发人员创建同名partial类进行代码合并
*/
using AutoMapper;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.ViewModels;

namespace XieCan.AspMVC.Mapper
{
    public partial class AutoMapperExtension
    {
        private static IMapper _Mapper = null;

        private static IMapper Maps
        {
            get
            {
                if (_Mapper == null)
                    _Mapper = AutoMapper();
                return _Mapper;
            }
        }

        public static IMapper AutoMapper()
        {
            return new MapperConfiguration(cfg =>
            {
                //忽略字段映射
                cfg.ShouldMapField = field => false;

                //蛇形命名法
                //cfg.SourceMemberNamingConvention = new PascalCaseNamingConvention();

                //驼峰命名法
                //cfg.DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();

                cfg.CreateMap<SimpleLoginViewModel, AccountModel>();

                cfg.CreateMap<AccountModel, SimpleLoginViewModel>();

                cfg.CreateMap<LoginResultViewModel, AccountModel>();

                cfg.CreateMap<AccountModel, LoginResultViewModel>();

                cfg.CreateMap<ArticleTypeSelectViewModel, ArticleTypeModel>();

                cfg.CreateMap<ArticleTypeModel, ArticleTypeSelectViewModel>();

                cfg.CreateMap<ArticleTypeUpdateViewModel, ArticleTypeModel>();

                cfg.CreateMap<ArticleTypeModel, ArticleTypeUpdateViewModel>();

                cfg.CreateMap<ArticleTypeInsertViewModel, ArticleTypeModel>();

                cfg.CreateMap<ArticleTypeModel, ArticleTypeInsertViewModel>();

                cfg.CreateMap<ArticleSelectViewModel, ArticleModel>();

                cfg.CreateMap<ArticleModel, ArticleSelectViewModel>();

                cfg.CreateMap<ArticleInsertViewModel, ArticleModel>();

                cfg.CreateMap<ArticleModel, ArticleInsertViewModel>();

                cfg.CreateMap<RoleViewModel, RoleModel>();

                cfg.CreateMap<RoleModel, RoleViewModel>();

            }).CreateMapper();
        }
    }
}

