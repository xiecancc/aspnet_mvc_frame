﻿using System;
using XieCan.AspMVC.Models;

namespace XieCan.AspMVC.IService
{
    public partial interface IAccountService
    {
        /// <summary>
        /// 账号登录
        /// </summary>
        /// <param name="account">账号信息</param>
        /// <returns>账号信息</returns>
        OperateResult<AccountModel> Login(AccountModel account);

        /// <summary>
        /// 检查账号是否存在
        /// </summary>
        /// <param name="account">账号信息</param>
        /// <returns>存在则返回对象，否则返回null</returns>
        OperateResult<AccountModel> CheckAccount(AccountModel account);

        /// <summary>
        /// 账号注册
        /// </summary>
        /// <param name="account">账号信息</param>
        /// <returns></returns>
        OperateResult<bool> Register(AccountModel account);
    }
}
