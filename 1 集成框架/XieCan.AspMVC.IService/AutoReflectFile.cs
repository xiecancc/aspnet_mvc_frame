/*
    作者：谢灿软件
    企鹅：492384481
    时间：2020-11-25 13:50:57
    说明：
        1.此文件根据XieCan.AspMVC.Entities项目中EntitiesContext类中的所有XieCanT6Property约束的属性自动生成
        2.需要提前编译XieCan.AspMVC.Entities项目，否则将生成失败或生成的代码不正确
        3.生成代码为partial接口，支持开发人员创建同名partial接口进行代码合并
*/
using XieCan.AspMVC.Log4net;
using XieCan.AspMVC.Models;
using XieCan.AspMVC.Reflections;

namespace XieCan.AspMVC.IService
{
    
    /// <summary>
    /// 日志信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "日志信息", Model = typeof(Log4Model))]
    public partial interface ILog4Service : IBaseService<Log4Model>
    {
    }

    /// <summary>
    /// 账号信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "账号信息", Model = typeof(AccountModel))]
    public partial interface IAccountService : IBaseService<AccountModel>
    {
    }

    /// <summary>
    /// 文章信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "文章信息", Model = typeof(ArticleModel))]
    public partial interface IArticleService : IBaseService<ArticleModel>
    {
    }

    /// <summary>
    /// 文章分类操作接口
    /// </summary>
    [XieCanT6Type(Summary = "文章分类", Model = typeof(ArticleTypeModel))]
    public partial interface IArticleTypeService : IBaseService<ArticleTypeModel>
    {
    }

    /// <summary>
    /// 权限信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "权限信息", Model = typeof(PermissionModel))]
    public partial interface IPermissionService : IBaseService<PermissionModel>
    {
    }

    /// <summary>
    /// 角色信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "角色信息", Model = typeof(RoleModel))]
    public partial interface IRoleService : IBaseService<RoleModel>
    {
    }

    /// <summary>
    /// 基本信息操作接口
    /// </summary>
    [XieCanT6Type(Summary = "基本信息", Model = typeof(UserInfoModel))]
    public partial interface IUserInfoService : IBaseService<UserInfoModel>
    {
    }
}
